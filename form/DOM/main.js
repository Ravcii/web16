function submitData(){
	var name = document.getElementById("name").value;
	var email = document.getElementById("email").value;
	var pass1 = document.getElementById("pass1").value;
	var pass2 = document.getElementById("pass2").value;
	var game = document.getElementById("disp").value;
	
	var out = document.getElementById("out");
	
	if( pass1 != pass2 ){
		out.textContent = "Пароли не совпадают. Повторите попытку!";
		return false;
	}
	
	out.textContent = name + " (" + email + ") вы были зарегистрированы в дисциплине \"" + dispToText(game) + "\"";
	
	return false;
}

function dispToText(id){
	switch(id){
		case "1":
			return "Hearthstone";
		case "2":
			return "Dota 2";
		case "3":
			return "CS: GO";
		case "4":
			return "FIFA 16";
		default:
			return "Жизнь";
	}
}